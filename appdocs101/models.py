from django.db import models
from datetime import datetime

from django.contrib.sessions.models import Session

# Create your models here.

""" Model to store user account details """
'''class User(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    password = models.CharField(max_length=200) # make sure to hash password before storing
    status = models.IntegerField(default=0) # 0 = 'ordinary user', 1 = 'admin'
    date_registered = models.DateTimeField('date registered')'''
  

""" Model to store software documentation details """
class Documentation(models.Model):
    title = models.TextField()
    software = models.CharField(max_length=200)
    version = models.CharField(max_length=200)
    operating_system = models.CharField(max_length=200)
    content = models.TextField()
    user = models.IntegerField()
    status = models.IntegerField(default=0) # 0 = 'unpublished', 1 = 'published'
    date_added = models.DateTimeField('date added')
    details = models.CharField(max_length=200, default = "version 1")
    collaborators_needed = models.BooleanField(default = False)

    def __unicode__(self):
	return self.title

""" Model to store software documentation wishlist/requests """
class Wishlist(models.Model):
    title = models.TextField()
    software = models.CharField(max_length=200)
    version = models.CharField(max_length=200)
    operating_system = models.CharField(max_length=200)
    content = models.TextField()
    user = models.IntegerField()
    status = models.IntegerField(default=0) # 0 = 'open', 1 = 'assigned/taken', 2 = 'granted' 
    date_added = models.DateTimeField('date added')

    def __unicode__(self):
	return self.title

    def accept(self, user):
	#transfer wishlist data to documentation
	doc = Documentation(title = self.title, software = self.software, 
	    version = self.version, operating_system = self.operating_system, 
	    user = user, status = 1, date_added = datetime.now())
	#wishlist = Wishlist.objects.filter(user = user)[0]
	#print wishlist.status

	#change wishlist status from open to taken
	self.status = 1
	self.save()

	#wishlist.status = 1
	#add to author list
	doc.authors_set.create(user = user, date_added = datetime.now())
 	wishlist.save()

	#print wishlist.status




""" Model to store software documentation comments """
'''class Documentation_Comments(models.Model):
    documentation = models.ForeignKey(Documentation)
    user = models.IntegerField()
    content = models.TextField()
    date_added = models.DateTimeField('date added')'''

""" Model to store software wishlist documentation comments """
class Wishlist_Comments(models.Model):
    wishlist = models.ForeignKey(Wishlist)
    user = models.IntegerField()
    content = models.TextField()
    date_added = models.DateTimeField('date added')

""" Model to store software documentation authors """
class Authors(models.Model):
    documentation = models.ForeignKey(Documentation)
    user = models.IntegerField()
    date_added = models.DateTimeField('date added')

    '''def __unicode__(self):
	return self.user'''

""" Model to store software documentation details """
class Documentation_Revisions(models.Model):
    documentation = models.ForeignKey(Documentation)
    title = models.TextField()
    software = models.CharField(max_length=200)
    version = models.CharField(max_length=200)
    operating_system = models.CharField(max_length=200)
    content = models.TextField()
    user = models.IntegerField()
    status = models.IntegerField() # 0 = 'unpublished', 1 = 'published'
    description = models.CharField(max_length=200)
    collaborators_needed = models.BooleanField(default = False)
    date_added = models.DateTimeField('date added')

    def __unicode__(self):
	return self.description











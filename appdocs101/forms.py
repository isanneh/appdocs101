from django import forms


class RegisterForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

class SigninForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())

class AddDocumentationForm(forms.Form):
    title = forms.CharField()
    software = forms.CharField()
    version = forms.CharField()
    operating_system = forms.CharField()
    status = forms.ChoiceField(widget = forms.Select(), choices = ([(0, 'Draft'), (1, 'Publish'), (2, 'Review')]))
    collaborators_needed = forms.BooleanField(widget = forms.CheckboxInput, required=False)


class AddWishlistForm(forms.Form):
    title = forms.CharField()
    software = forms.CharField()
    version = forms.CharField()
    operating_system = forms.CharField()
    content = forms.CharField(widget = forms.Textarea())

class CommentForm(forms.Form):
    content = forms.CharField(widget = forms.Textarea())

class ResourceForm(forms.Form):
    title = forms.CharField()
    link = forms.URLField()
    description = forms.CharField(widget = forms.Textarea())






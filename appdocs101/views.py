# Create your views here.

from django.http import HttpResponse

from django.shortcuts import render
from django.contrib.auth.models import User

from django.contrib.auth import get_user_model #django user model

from appdocs101.forms import * #file where forms are located

from appdocs101.models import * #database

from django.contrib import messages #flash messages
from django.shortcuts import redirect

from django.http import HttpResponseRedirect

from django.contrib.auth import authenticate, login, logout

from datetime import datetime # date library

from django.shortcuts import get_object_or_404 #404 error if the data requested is not in the database

from django.core.urlresolvers import reverse

def user(request, user_id):
    return HttpResponse("You're looking at user %s" % user_id)

""" user registration """
def register(request):
    if request.method == 'POST': #if the form has been submitted
	form = RegisterForm(request.POST)
	if form.is_valid(): #form is validated
	    # add user to database

	    user = get_user_model().objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])

	    user.first_name = form.cleaned_data['first_name']
	    user.last_name = form.cleaned_data['last_name']
	    user.is_active = True
	    user.save()

	    messages.add_message(request, messages.INFO, "You have been successfully registered!!")
	    #render(request,  'appdocs101/signin.html')
	    return HttpResponseRedirect(reverse('africandesigns:signin'))

    else:
        form = RegisterForm()
    return render(request, 'appdocs101/register.html',  {'form': form, 'title': 'Register'})


""" user signin """
def signin(request):
    if request.method == 'POST':
	form = SigninForm(request.POST)
	if form.is_valid():
	    # check email and password match
	    query_user = get_user_model().objects.filter(email = form.cleaned_data['email'])
	    if len(query_user) == 1: 
		user = query_user[0]
		if user.check_password(form.cleaned_data['password']) == True: # authentication successful
		    if user.is_active: # account is active
		        messages.add_message(request, messages.INFO, "You have successfully signed in!")
		        #store user in session to indicate user is logged in
		        request.session['user'] = user
    		        return HttpResponseRedirect(reverse('appdocs101:add_doc'))
		    else: # account is inactive
	    	        messages.add_message(request, messages.INFO, "Sorry, you cannot login because you account has been deactivated!")		
	        else: # account is inactive
	            messages.add_message(request, messages.INFO, "Sorry, email address and/or password is incorrect!")

    else:
	form = SigninForm()
    return render(request, 'appdocs101/signin.html', {'form': form, 'title': 'Signin'})


""" Logout user """ 
def logout(request):
    #remove user from session to logout user if user is currently logged in
    if 'user' in request.session:
	del request.session['user']
	messages.add_message(request, messages.INFO, "You have successfully logged out!")
    #logout(request)
    #messages.add_message(request, messages.INFO, "You have successfully logged out!")
    #return render(request, 'appdocs101/logout.html')
    return HttpResponseRedirect(reverse('appdocs101:signin'))

""" adding software documentation/tutorial """
def add_doc(request):
    #check if form is submitted
    if request.method == 'POST':
        form = AddDocumentationForm(request.POST)
	#check if validation is successful
	if form.is_valid():
	    #add documentation details to database
	    doc = Documentation(title = form.cleaned_data['title'], software = form.cleaned_data['software'], version = form.cleaned_data['version'], operating_system = form.cleaned_data['operating_system'], content = request.POST['content'], user = request.session['user'].id, status = form.cleaned_data['status'], collaborators_needed = form.cleaned_data['collaborators_needed'], date_added = datetime.now())
	    doc.save()
	    messages.add_message(request, messages.INFO, "Your documentation was successfully added!")
	    #redirect to docs page      
	    HttpResponseRedirect(reverse('appdocs101:docs'))
    #form is not submitted, so display blank form
    else:
        form = AddDocumentationForm()
    return render(request, 'appdocs101/add_doc.html', {'form': form, 'title': 'Add Documentation'})


""" adding software documentation/tutorial """
def add_doc2(request):
    #check if form is submitted
    if request.method == 'POST':
        form = AddDocumentationForm(request.POST)
	#check if validation is successful
	if form.is_valid():
	    #add documentation details to database
	    doc = Documentation(title = form.cleaned_data['title'], software = form.cleaned_data['software'], version = form.cleaned_data['version'], operating_system = form.cleaned_data['operating_system'], content = request.POST['editor_content'], user = request.session['user'].id, status = form.cleaned_data['status'], collaborators_needed = form.cleaned_data['collaborators_needed'], date_added = datetime.now())
	    doc.save()
	    messages.add_message(request, messages.INFO, "Your documentation was successfully added!")
	    #redirect to docs page      
	    HttpResponseRedirect(reverse('appdocs101:docs'))
    #form is not submitted, so display blank form
    else:
        form = AddDocumentationForm()
    return render(request, 'appdocs101/add_doc2.html', {'form': form, 'title': 'Add Documentation'})

""" adding software wishlist documentation/tutorial """
def add_wishlist(request):
    #check if form is submitted
    if request.method == 'POST':
        form = AddWishlistForm(request.POST) 
	#check if validation is successful
	if form.is_valid():
	    #add documentation details to database
	    wishlist = Wishlist(title = form.cleaned_data['title'], software = form.cleaned_data['software'], version = form.cleaned_data['version'], operating_system = form.cleaned_data['operating_system'], content = form.cleaned_data['content'], user = request.session['user'].id, status = 0, date_added = datetime.now())
	    wishlist.save()
	    messages.add_message(request, messages.INFO, "Your wishist was successfully added.  You will be emailed when someone accepts your request!")
	    #redirect to wishlists page
	    HttpResponseRedirect(reverse('appdocs101:wishlists'))
    #form is not submitted, so display blank form
    else:
        form = AddWishlistForm()
    return render(request, 'appdocs101/add_wishlist.html', {'form': form, 'title': 'Add Wishlist'})

""" displaying list of documentations/tutorials """
def docs(request):
    #query to return all published documentations
    docs = Documentation.objects.filter(status = 1)
    return render(request, 'appdocs101/docs.html', {'title': 'Docs', 'docs': docs})

""" displaying wishlists of documentations users need a basic introduction/tutorial on """
def wishlists(request):
    #query to return all wishlists that have not yet been accepted
    wishlists = Wishlist.objects.filter(status = 0)
    return render(request, 'appdocs101/wishlists.html', {'title': 'Wishlist', 'wishlists': wishlists})

""" adding comments """
def add_comment(request, doc_id):
    #check if form is submitted
    if request.method == 'POST':
        form2 = CommentForm(request.POST)
	#check if validation is successful
	if form2.is_valid():

	    #query to return documentation with id "doc_id"
	    doc = Documentation.objects.get(id = doc_id)
	    #query to return author
	    user = get_user_model().objects.get(id = doc.user)

	    #add comment details to database
	    comment = doc.documentation_comments.create(user = user, content = form2.cleaned_data['content'], date_added = datetime.now())
	    comment.save()
	    messages.add_message(request, messages.INFO, "Your comment was successfully added!")
    #form is not submitted, so display blank form
    else:
        form2 = CommentForm()
    return render(request, 'appdocs101/add_comment.html', {'form': form2})


""" documentation/tutorial page """
def doc(request, doc_id): 
    #query to return documentation with id "doc_id", or raise Http404 error if data doesn't exist in database
    doc = get_object_or_404(Documentation,id = doc_id)
    #query to return author #Documentation.objects.get(id = doc_id)
    user = get_user_model().objects.get(id = doc.user)

    #get revision data
    revisions = doc.documentation_revisions_set.all()
    return render(request, 'appdocs101/doc.html', {'title': 'Docs', 'doc': doc, 'user': user, 'com':  add_comment(request, doc_id), 'revisions': revisions, })


""" documentation/tutorial page """
def doc22(request, doc_id): 
    #query to return documentation with id "doc_id", or raise Http404 error if data doesn't exist in database
    doc = get_object_or_404(Documentation,id = doc_id)
    #query to return author #Documentation.objects.get(id = doc_id)
    user = get_user_model().objects.get(id = doc.user)

    #get revision data
    revisions = doc.documentation_revisions_set.all()
    return render(request, 'appdocs101/doc22.html', {'title': 'Docs', 'doc': doc, 'user': user, 'com':  add_comment(request, doc_id), 'revisions': revisions, })

""" wishlist page """
def wishlist(request, wishlist_id):
    #query to return wishlist with id "wishlist_id"
    wishlist = Wishlist.objects.get(id = wishlist_id)
    #query to return user who added the wishlist
    user = get_user_model().objects.get(id = wishlist.user)
    return render(request, 'appdocs101/wishlist.html', {'title': 'Wishlist', 'wishlist': wishlist, 'user': user})


""" accept wishlist """
def accept_wishlist(request):
    if request.method == "POST":
        #query to return wishlist with id "wishlist_id"
        wishlist = Wishlist.objects.get(id = request.POST['wishlist_id'])
        doc = wishlist.accept(request.session['user'].id)
        messages.add_message(request, messages.INFO, "You accepted the wishlist!")
    return HttpResponseRedirect(reverse('appdocs101:add_doc'))

""" displaying user's list of contributions """
def contributions(request):
    #query to return all documentations
    docs = Documentation.objects.filter(user = request.session['user'].id)
    #query to return all wishlists that have not yet been accepted
    wishlists = Wishlist.objects.filter(user = request.session['user'].id)
    return render(request, 'appdocs101/contributions.html', {'title': 'Contributions', 'docs': docs, 'wishlists': wishlists,})


""" displaying user's list of contributions """
def edit_doc(request, doc_id):
    #query to return all documentations
    docs = Documentation.objects.filter(user = request.session['user'].id)
    #query to return all wishlists that have not yet been accepted
    wishlists = Wishlist.objects.filter(user = request.session['user'].id)
    return render(request, 'appdocs101/contributions.html', {'title': 'Contributions', 'docs': docs, 'wishlists': wishlists,})


""" editing software documentation/tutorial """
def edit_doc2(request, doc_id):
    #check if form is submitted
    if request.method == 'POST':
        form = AddDocumentationForm(request.POST)
	#check if validation is successful
	if form.is_valid():

	    doc = Documentation.objects.get(id = doc_id)

	    #add previous data to revision history
	    #description = "version " + str(doc.documentation_revisions_set.count() + 1)
	    doc.documentation_revisions_set.create(title = doc.title, software = doc.software, version = doc.version, operating_system = doc.operating_system, content = doc.content, user = doc.user , status = doc.status, description = doc.details, collaborators_needed = doc.collaborators_needed, date_added = doc.date_added)

	    #add documentation details to database
	    doc.title = form.cleaned_data['title']
	    doc.software = form.cleaned_data['software']
	    doc.version = form.cleaned_data['version'] 
	    doc.operating_system = form.cleaned_data['operating_system'] 
	    doc.status = form.cleaned_data['status']
	    doc.content = request.POST['content']
	    doc.details = "version " + str(doc.documentation_revisions_set.count() + 1)
	    doc.collaborators_needed = form.cleaned_data['collaborators_needed']
	    doc.save()

	    #doc = Documentation(title = form.cleaned_data['title'], software = form.cleaned_data['software'], version = form.cleaned_data['version'], operating_system = form.cleaned_data['operating_system'], content = request.POST['content'], user = request.session['user'].id, status = form.cleaned_data['status'], date_added = datetime.now())
	    #doc.save()
	    messages.add_message(request, messages.INFO, "Your documentation was successfully modified!")
	    #redirect to docs page
	    HttpResponseRedirect(reverse('appdocs101:docs'))
    #form is not submitted, so display blank form
    else:
	#get doc details
	doc = Documentation.objects.get(id = doc_id)
        #add details to form

	data = {
	    'title': doc.title,
	    'software': doc.software,
	    'version': doc.version,
	    'operating_system': doc.operating_system,
	    'status': doc.status,
	}

        form = AddDocumentationForm(data)
    return render(request, 'appdocs101/edit_doc.html', {'form': form, 'title': 'Edit Documentation', 'content': doc.content, 'doc_id': doc_id,})

""" documentation/tutorial revision page """
def revision(request, revision_id): 
    #query to return documentation with id "doc_id", or raise Http404 error if data doesn't exist in database
    revision = get_object_or_404(Documentation_Revisions, id = revision_id)
    user = get_user_model().objects.get(id = revision.user)

    return render(request, 'appdocs101/revision.html', {'title': 'Revision', 'revision': revision, 'user': user, })



""" restoring previous documentation/tutorial """
def restore(request, revision_id):
	    revision = Documentation_Revisions.objects.get(id = revision_id)
	    doc = revision.documentation

	    #add previous data to revision history
	    #description = "restored version " + str(revision.id)
	    doc.documentation_revisions_set.create(title = doc.title, software = doc.software, version = doc.version, operating_system = doc.operating_system, content = doc.content, user = doc.user , status = doc.status, description = doc.details, collaborators_needed = doc.collaborators_needed, date_added = doc.date_added)

	    #restore selected version
	    doc.title = revision.title
	    doc.software = revision.software
	    doc.version = revision.version
	    doc.operating_system = revision.operating_system
	    doc.status = revision.status
	    doc.content = revision.content
	    doc.details = "version " + str(doc.documentation_revisions_set.count() + 1) + ": restored version " + str(revision.id)
	    doc.collaborators_needed = revision.collaborators_needed
	    doc.save()

	    messages.add_message(request, messages.INFO, "Documentation was successfully restored!")

	    #redirect to docs page
	    HttpResponseRedirect(reverse('appdocs101:docs'))

""" displaying list of documentations/tutorials in progress """
def pending_docs1(request):
    #query to return all unpublished documentations
    docs = Documentation.objects.filter(status = 0)
    return render(request, 'appdocs101/pending_docs.html', {'title': 'Pending Docs', 'docs': docs})

""" displaying list of documentations/tutorials under review """
def review_docs1(request):
    #query to return all documentations under review
    docs = Documentation.objects.filter(status = 2)
    return render(request, 'appdocs101/review_docs.html', {'title': 'Review Docs', 'docs': docs})






























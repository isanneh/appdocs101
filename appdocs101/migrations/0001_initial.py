# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Documentation'
        db.create_table(u'appdocs101_documentation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('software', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('operating_system', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('user', self.gf('django.db.models.fields.IntegerField')()),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('details', self.gf('django.db.models.fields.CharField')(default='version 1', max_length=200)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'appdocs101', ['Documentation'])

        # Adding model 'Wishlist'
        db.create_table(u'appdocs101_wishlist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('software', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('operating_system', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('user', self.gf('django.db.models.fields.IntegerField')()),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'appdocs101', ['Wishlist'])

        # Adding model 'Wishlist_Comments'
        db.create_table(u'appdocs101_wishlist_comments', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('wishlist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appdocs101.Wishlist'])),
            ('user', self.gf('django.db.models.fields.IntegerField')()),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'appdocs101', ['Wishlist_Comments'])

        # Adding model 'Authors'
        db.create_table(u'appdocs101_authors', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('documentation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appdocs101.Documentation'])),
            ('user', self.gf('django.db.models.fields.IntegerField')()),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'appdocs101', ['Authors'])

        # Adding model 'Documentation_Revisions'
        db.create_table(u'appdocs101_documentation_revisions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('documentation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['appdocs101.Documentation'])),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('software', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('operating_system', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('user', self.gf('django.db.models.fields.IntegerField')()),
            ('status', self.gf('django.db.models.fields.IntegerField')()),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'appdocs101', ['Documentation_Revisions'])


    def backwards(self, orm):
        # Deleting model 'Documentation'
        db.delete_table(u'appdocs101_documentation')

        # Deleting model 'Wishlist'
        db.delete_table(u'appdocs101_wishlist')

        # Deleting model 'Wishlist_Comments'
        db.delete_table(u'appdocs101_wishlist_comments')

        # Deleting model 'Authors'
        db.delete_table(u'appdocs101_authors')

        # Deleting model 'Documentation_Revisions'
        db.delete_table(u'appdocs101_documentation_revisions')


    models = {
        u'appdocs101.authors': {
            'Meta': {'object_name': 'Authors'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {})
        },
        u'appdocs101.documentation': {
            'Meta': {'object_name': 'Documentation'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'details': ('django.db.models.fields.CharField', [], {'default': "'version 1'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.documentation_revisions': {
            'Meta': {'object_name': 'Documentation_Revisions'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist': {
            'Meta': {'object_name': 'Wishlist'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist_comments': {
            'Meta': {'object_name': 'Wishlist_Comments'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'wishlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Wishlist']"})
        }
    }

    complete_apps = ['appdocs101']
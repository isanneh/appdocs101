# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Documentation.details'
        db.add_column(u'appdocs101_documentation', 'details',
                      self.gf('django.db.models.fields.CharField')(default='version 1', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Documentation.details'
        db.delete_column(u'appdocs101_documentation', 'details')


    models = {
        u'appdocs101.authors': {
            'Meta': {'object_name': 'Authors'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {})
        },
        u'appdocs101.documentation': {
            'Meta': {'object_name': 'Documentation'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'details': ('django.db.models.fields.CharField', [], {'default': "'version 1'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.documentation_revisions': {
            'Meta': {'object_name': 'Documentation_Revisions'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist': {
            'Meta': {'object_name': 'Wishlist'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist_comments': {
            'Meta': {'object_name': 'Wishlist_Comments'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'wishlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Wishlist']"})
        }
    }

    complete_apps = ['appdocs101']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Documentation.collaborators_needed'
        db.add_column(u'appdocs101_documentation', 'collaborators_needed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Documentation_Revisions.collaborators_needed'
        db.add_column(u'appdocs101_documentation_revisions', 'collaborators_needed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Documentation.collaborators_needed'
        db.delete_column(u'appdocs101_documentation', 'collaborators_needed')

        # Deleting field 'Documentation_Revisions.collaborators_needed'
        db.delete_column(u'appdocs101_documentation_revisions', 'collaborators_needed')


    models = {
        u'appdocs101.authors': {
            'Meta': {'object_name': 'Authors'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {})
        },
        u'appdocs101.documentation': {
            'Meta': {'object_name': 'Documentation'},
            'collaborators_needed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'details': ('django.db.models.fields.CharField', [], {'default': "'version 1'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.documentation_revisions': {
            'Meta': {'object_name': 'Documentation_Revisions'},
            'collaborators_needed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'documentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Documentation']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist': {
            'Meta': {'object_name': 'Wishlist'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operating_system': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'software': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'appdocs101.wishlist_comments': {
            'Meta': {'object_name': 'Wishlist_Comments'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.IntegerField', [], {}),
            'wishlist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['appdocs101.Wishlist']"})
        }
    }

    complete_apps = ['appdocs101']
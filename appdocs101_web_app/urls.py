from django.conf.urls import patterns, include, url

#from appdocs101_web_app import views

from appdocs101 import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'appdocs101_web_app.views.home', name='home'),
    # url(r'^appdocs101_web_app/', include('appdocs101_web_app.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),

    #(r'^accounts/', include('userena.urls')),

    url(r'^appdocs101/', include('appdocs101.urls', namespace="appdocs101")), 

    url(r'^$', views.docs, name='docs'),

)  


